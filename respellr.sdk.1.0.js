/**
 * @typedef {string} linkToGoogleDoc
 * @typedef {Object} Messages
 * @property {string} CLOSE_ALL_MODALS
 * @property {string} SKIP_IT
 * @property {string} IS_GAME_MODAL_OPEN_TOGGLE
 * @property {string} WORD_WIN
 * @property {string} DICTATION
 * @property {string} CHALLENGE_WORD_INDEX
 * @property {string} IS_ALL_GAMES_FINISHED
 * @property {string} FINISH_GAME
 * @property {string} START_GAME
 */

/**
 * A constant that holds the URL for Google Docs
 * @type {linkToGoogleDoc}
 */
const linkToGoogleDoc = "https://docs.google.com/";

/** This logger is used for debugging */
class Logger {
  /**
   * The log method outputs a message to the web console.
   * @param {...unknown} args - The arguments to print to the console.
   */
  log(...args) {
    console.log(
      `[sdk][${new Date().toISOString()}][${this.name}][info]: `,
      ...args
    );
  }

  /**
   * The verbose method outputs a detailed message to the web console.
   * @param {...unknown} args - The arguments to print to the console.
   */
  verbose(...args) {
    console.log(
      `[sdk][${new Date().toISOString()}][${this.name}][verbose]: `,
      ...args
    );
  }

  /**
   * The error method outputs an error message to the web console.
   * @param {...unknown} args - The arguments to print to the console.
   */
  error(...args) {
    console.error(
      `[sdk][${new Date().toISOString()}][${this.name}][error]: `,
      ...args
    );
  }
}

/** This logger is used for debugging
 * @type {Logger}
 */
let logger = new Logger("respellr.sdk");

/**
 * Represents the messages used in the application.
 * Each key in the object represents a unique message.
 * @example
 * // Message for closing all modals
 * messages.CLOSE_ALL_MODALS // "close all modals"
 * // Message for skipping
 * messages.SKIP_IT // "skip it"
 * // Message for toggling game modal open status
 * messages.IS_GAME_MODAL_OPEN_TOGGLE // "is game modal open toggle"
 * // Message for word win
 * messages.WORD_WIN // "word win"
 * // Message for dictation
 * messages.DICTATION // "dictation"
 * // Message for challenge word index
 * messages.CHALLENGE_WORD_INDEX // "challenge word index"
 * // Message for checking if all games are finished
 * messages.IS_ALL_GAMES_FINISHED // "is all games finished"
 * // Message for finishing game
 * messages.FINISH_GAME // "finish game"
 * // Message for starting game
 * messages.START_GAME // "start game"
 * @type {Messages}
 */
const messages = {
  CLOSE_ALL_MODALS: "close all modals",
  SKIP_IT: "skip it",
  IS_GAME_MODAL_OPEN_TOGGLE: "is game modal open toggle",
  WORD_WIN: "word win",
  DICTATION: "dictation",
  CHALLENGE_WORD_INDEX: "challenge word index",
  IS_ALL_GAMES_FINISHED: "is all games finished",
  FINISH_GAME: "finish game",
  START_GAME: "start game",
};

/**
 * @param {sendMessageToGameTypes} message
 * @returns {void}
 */
const senMessageToParent = (message) => {
  window.parent.postMessage(message, "*");
};

(function (window) {
  const RespellrSDK = {
    /**
     * @returns {void}
     */
    closeAllModals() {
      senMessageToParent({
        type: messages.CLOSE_ALL_MODALS,
      });
    },
    /**
     * @param {DictaionType} dictation
     * @returns {void}
     */
    skipIt(dictation) {
      senMessageToParent({
        type: messages.SKIP_IT,
        dictation,
      });
    },
    /**
     * @param {boolean} isGameModalOpenToggle
     * @returns {void}
     */
    isGameModalOpenToggle(isGameModalOpenToggle) {
      senMessageToParent({
        type: messages.IS_GAME_MODAL_OPEN_TOGGLE,
        isGameModalOpenToggle,
      });
    },
    /**
     * @param {handleWordWinItems} wordWinItems
     * @returns {void}
     */
    wordWin(wordWinItems) {
      senMessageToParent({
        type: messages.WORD_WIN,
        wordWinItems,
      });
    },
    /**
     * @returns {Promise<{object: handleWordWinItems}>}
     */
    getDictation() {
      const id = Math.round(Math.random() * 10000000);

      return new Promise((resolve) => {
        senMessageToParent({
          type: messages.DICTATION,
          id,
        });

        const handler = (event) => {
          if (
            event.data.type === `${messages.DICTATION}:response` &&
            event.data.id === id
          ) {
            resolve(event.data);
            window.removeEventListener("message", handler);
          } else {
            return;
          }
        };

        window.addEventListener("message", handler);
      });
    },
    /**
     * @returns {Promise<{dictation: DictaionType}>}
     */
    getChallengeWordIndex() {
      const id = Math.round(Math.random() * 10000000);

      return new Promise((resolve) => {
        senMessageToParent({
          type: messages.CHALLENGE_WORD_INDEX,
          id,
        });

        const handler = (event) => {
          if (
            event.data.type === `${messages.CHALLENGE_WORD_INDEX}:response` &&
            event.data.id === id
          ) {
            resolve(event.data);
            window.removeEventListener("message", handler);
          } else {
            return;
          }
        };

        window.addEventListener("message", handler);
      });
    },
    /**
     * @returns {Promise<{challengeWordIndex: number}>}
     */
    getIsAllGamesFinished() {
      const id = Math.round(Math.random() * 10000000);

      return new Promise((resolve) => {
        senMessageToParent({
          type: messages.IS_ALL_GAMES_FINISHED,
          id,
        });

        const handler = (event) => {
          if (
            event.data.type === `${messages.IS_ALL_GAMES_FINISHED}:response` &&
            event.data.id === id
          ) {
            resolve(event.data);
            window.removeEventListener("message", handler);
          } else {
            return;
          }
        };

        window.addEventListener("message", handler);
      });
    },
    /**
     * @returns {Promise<{isFinishGame: boolean}>}
     */
    getFinishGame() {
      const id = Math.round(Math.random() * 10000000);

      return new Promise((resolve) => {
        senMessageToParent({
          type: messages.FINISH_GAME,
          id,
        });

        const handler = (event) => {
          if (
            event.data.type === `${messages.FINISH_GAME}:response` &&
            event.data.id === id
          ) {
            resolve(event.data);
            window.removeEventListener("message", handler);
          } else {
            return;
          }
        };

        window.addEventListener("message", handler);
      });
    },
    /**
     * @param {sendMessageToGameTypes} sendMessageToGameTypes
     * @returns {void}
     */
    sendMessageToGame({ data, iframeDiv }) {
      iframeDiv.contentWindow.postMessage(data, "*");
    },
  };

  window.RespellrSDK = RespellrSDK;
})(window);
